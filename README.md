# README #

WP Plugin Show Available Days on Calendar

### Show Custom Created Available Days on Calendar ###

## * Simply add it from widgets, it will work only in single pages of posts ##

![2016-03-16_1224.png](https://bitbucket.org/repo/xpnqdX/images/3980703492-2016-03-16_1224.png)


## * Ajax fronend features for logged in users allows to add or remove days in calendar by click to any number of the calendar ##

![2016-03-16_1230.png](https://bitbucket.org/repo/xpnqdX/images/1138835980-2016-03-16_1230.png)


***all screenshots you can find in source folder*****