<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ShowAvDaysModel{
    
    
    private $db;
    private $table;
    
    public function __construct() {
        
        global $wpdb;        
        $this->db = $wpdb;
        
        $this->table = 'show_av_days';
    }
    
    public function insertCalendarRow( $data ){    
        
        return $this->db->insert( $this->table, $data );
        
    }
    public function selectDataID( $post_id, $date ) {    
        
        $query = "SELECT `id` FROM " . $this->table . " WHERE `post_id` = ".$post_id." AND `date`= '".$date."'";
        $result = $this->db->get_row( $query );        
       
        return $result ;
    }
    
    public function deleteDataRow($id){
        
       $this->db->delete($this->table, array('id' => (int) $id));
        
    }

    public function calendarContent( $month = NULL, $year = NULL, $postID = NULL ){        
                   
        $content = self::loadCalendarContent( $month, $year, $postID );
        
        return $content;
    }
    
    public function loadCalendarContent( $m = NULL, $y = NULL, $id = NULL ) {
       
        $date = time();
        
        $day   = date('d', $date);
        $month = ($m != NULL) ? $m : date('m', $date);
        $year  = ($y != NULL) ? $y : date('Y', $date);        
                
        $first_day = mktime(0,0,0,$month,1,$year);        
        $title = date( 'F', $first_day );
        $day_of_week = date('D', $first_day);
        
        $postId = ( $id != NULL ) ? $id : get_the_ID();
        
        $adminClass = "";
        if ( is_user_logged_in() ) { 
            $adminClass = "admin-mode"; 
        }
        
        switch ( $day_of_week ){
            case "Sun": $blank = 0; break;
            case "Mon": $blank = 1; break;
            case "Tue": $blank = 2; break;
            case "Wed": $blank = 3; break;
            case "Thu": $blank = 4; break;
            case "Fri": $blank = 5; break;
            case "Sat": $blank = 6; break;
        }
        
        $days_in_month = cal_days_in_month(0, $month, $year);
        $content = '';
        $content .= "<table class='show_av_days_table ".$adminClass."'>";
        
        $content .= "<tr>";
        $content .= "<th class='controls'><a data-month-num=$month data-action='next_month' class='prev' >&lt;</a></th>";
        $content .= "<th colspan=5 class='month-title' > $title <span id='show-year' data-year-num='$year'>$year</span> </th>";
        $content .= "<th class='controls'><a data-month-num=$month data-action='next_month' class='next' >&gt;</a></th>";
        $content .= "</tr>";
        
        $content .= "<tr>";
        $content .= "<td><span class='weeks'>S</span></td>";
        $content .= "<td><span class='weeks'>M</span></td>";
        $content .= "<td><span class='weeks'>T</span></td>";
        $content .= "<td><span class='weeks'>W</span></td>";
        $content .= "<td><span class='weeks'>T</span></td>";
        $content .= "<td><span class='weeks'>F</span></td>";
        $content .= "<td><span class='weeks'>S</span></td>";
        $content .= "</tr>";
        
        $day_count = 1;
        $content .= "<tr>";
        
        while ( $blank > 0 ){
            $content .= "<td></td>";
            $blank = $blank - 1;
            $day_count++;
        }
        
        if ( $month < 10 && substr( $month, 0, 1 ) != 0 ){
            $month = '0'.$month;
        }
        
        $day_num = 1;
        while ( $day_num <= $days_in_month ){
            
            $dayNumData = $day_num .'-'.$month.'-'.$year;
            
            $getReservedDays = self::selectDataID( $postId, $dayNumData );
            
            $class = ($getReservedDays != NULL) ? 'not-available' : "available";
            $rowId = ($getReservedDays != NULL) ? $getReservedDays->id : "none";            
            
            $content .= "<td data-id='".$rowId."' data-day='".$dayNumData."' class='calDay ".$class."'><span class='days'>$day_num</span></td>";
            
            $day_num++;
            $day_count++;
            
            if( $day_count > 7 ){
                $content .= "</tr><tr>";
                $day_count = 1;
            }            
        }
        
        while ( $day_count > 1 && $day_count <= 7 ){
            $content .= "<td></td>";
            $day_count++;
        }        
        
        $content .= "</tr>";
        $content .= "</table>";
        
        return $content;
    }
    
}