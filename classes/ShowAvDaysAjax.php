<?php

class ShowAvDaysAjax {

    public function setAdminAjaxActions() {

        //month next and prev function for all users need to work
        add_action( 'wp_ajax_nopriv_next_month', 'next_month' ); // ajax for not logged in users
        add_action('wp_ajax_next_month', 'next_month');
        function next_month() {    
            
            $month = (int) $_GET['month'];
            $year  = (int) $_GET['year'];
            $postId = (int) $_GET['id'];
            
            $model = new ShowAvDaysModel();  
            echo $model->calendarContent( $month, $year, $postId );
            die();
        }
        
        //We dont need insert data when user is not logged in
        //add_action( 'wp_ajax_nopriv_insert_row', 'insert_row' ); // ajax for not logged in users
        add_action('wp_ajax_insert_row', 'insert_row');
        function insert_row() {            
            
            $day = $_GET['date'];
            $postId = $_GET['id'];
            
            $data = array(
                        'post_id' => $postId,
                        'date' => $day,
                    );
            
            $model = new ShowAvDaysModel();            
            $model->insertCalendarRow( $data );
            
            die();
        }
        
        add_action('wp_ajax_delete_row', 'delete_row');
        function delete_row($id) {
            
            $id = $_GET['id'];
            
            $model = new ShowAvDaysModel();            
            $model->deleteDataRow( $id );
            
            die();
        }
        
        
        

    }
}