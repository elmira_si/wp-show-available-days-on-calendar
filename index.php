<?php

/*
  Plugin Name: Show Available Days on Calendar
  Plugin URI: https://bitbucket.org/elmira_k82/wp-show-available-days-on-calendar/src/
  Description: Show or Create Available Days on Calendar
  Version: 1.0
  Author: Elmira K.
  Author URI: http://testings.info
 */

ini_set('display_errors', 1);
error_reporting(E_ERROR | E_WARNING);

register_activation_hook(__FILE__, 'show_av_days_install');

function show_av_days_install() {

    global $wpdb;

    $show_av_days = "CREATE TABLE IF NOT EXISTS `show_av_days` (
        `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
        `post_id` int(15) unsigned NOT NULL,
        `date` varchar(60) COLLATE utf8_bin DEFAULT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1";    

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    dbDelta($show_av_days);
   
}

register_deactivation_hook( __FILE__, 'myplugin_deactivate' );

function myplugin_deactivate() {
    
    global $wpdb;
    $tables = array(
        'show_av_days'       
    );
    
    foreach($tables as $table) {
        $wpdb->query("DROP TABLE ".$table);
    }
}

spl_autoload_register(function ($class) {
    $file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . $class . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
});


$ajax = new ShowAvDaysInit;