//$ = jQuery.noConflict();
jQuery(document).ready(function() {
   

//    jQuery(document).on('click', 'a[data-action]', function(ev) {
//        var action = ajaxUrl + '?action=' + jQuery(this).attr('data-page-action');
//
//        jQuery.get(action, function(html) {
//            jQuery('.tjd-content').html('');
//            jQuery('.tjd-content').html(html);
//        });
//        ev.preventDefault();
//
//    });
    
    jQuery(document).on('click', 'a[data-action]', function(ev) {
        
        var month = jQuery(this).attr('data-month-num');
        var year = jQuery('#show-year').attr('data-year-num');        
        var action = jQuery(this).attr('data-action');
        var dataId = jQuery('#post-id').attr('data-post-id');
        
        var nextOrPrev = jQuery(this).attr('class');
        if ( nextOrPrev == 'next' ){
            month = parseInt(month) + 1;
            if ( month > 12 ){
                month = 1;
                year = parseInt(year) +1;
            } 
        } else {
            month = parseInt(month) - 1;            
            if ( month < 1 ){
                month = 12;
                year = parseInt(year) - 1;
            } 
        }
        var url = ajaxUrl + '?action=' + action + '&month=' + month + '&year=' + year + '&id=' + dataId;
        
        jQuery.get(
                url,
                function(html) {
                    if ( html ){
                        jQuery('.show_widget_calendar').html();
                        jQuery('.show_widget_calendar').html(html);
                    }
                });
        
        ev.preventDefault(); 
    });
    
    
    jQuery(document).on('click', '.admin-mode [data-day]', function(ev) {
        
        var dataDay = jQuery(this).attr('data-day');
        var dataId = jQuery(this).attr('data-id');
        var dataPostId = jQuery('#post-id').attr('data-post-id');
        
        var url = "";
        
        if ( dataId == "none" ){
            url = ajaxUrl + '?action=insert_row&date='+dataDay+'&id='+dataPostId;
            jQuery(this).attr('data-id', '');
            jQuery(this).removeClass('available').addClass('not-available');
        }else{
            url = ajaxUrl + '?action=delete_row&id='+dataId;
            jQuery(this).attr('data-id', 'none');
            jQuery(this).removeClass('not-available').addClass('available');
        }
       
        jQuery.get(url);
        
        ev.preventDefault(); 
    });
    
    

});



